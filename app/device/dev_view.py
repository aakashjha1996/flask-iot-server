from flask import Blueprint, request
from flask.views import MethodView

from app.auth.helper import response
from app.auth.models import User
from dev_model import device

dev = Blueprint('device', __name__)


class configure_devices(MethodView):
    def post(self):
        """Adding devices for each user"""
        if request.content_type == 'application/json':
            auth_header = request.headers.get('Authorization')
            if auth_header:
                try:
                    auth_token = auth_header.split(" ")[1]
                except IndexError:
                    return response('failed', 'Provide a valid auth token', 403)
                else:
                    user_id = User.decode_auth_token(auth_token)
                    dev_data = request.get_json()
                    dev_name = dev_data.get('devname')
                    dev_topic = dev_data.get('devtopic')
                    user = User.get_by_id(user_id)
                    if user:
                        message =device(dev_name,dev_topic).save_dev(user)
                        return response('Success',message,200)
                    return response('Failed','user doest Exist',400)
            return response('failed', 'Provide an authorization header', 403)
        return response('failed', 'Content-type must be json', 400)

    def get(self):
        """Sending back devices of each user"""
        auth_header = request.headers.get('Authorization')
        if auth_header:
                try:
                    auth_token = auth_header.split(" ")[1]
                except IndexError:
                    return response('failed', 'Provide a valid auth token', 403)
                else:
                    user_id = User.decode_auth_token(auth_token) #change here
                    user = User.get_by_id(user_id)
                    if user:
                        user_devices=[]
                        if not user.devices:
                            return response('Success','No device present for user',400)
                        for dev in user.devices:
                            user_devices.append(dev.get_dev_data())
                        return response('Success', user_devices, 200)
                    return response('Failed', 'user doest Exist', 400)
        return response('failed', 'Provide an authorization header', 403)

class user_get_monitor_devices(MethodView):

    def get(self):
        auth_header = request.headers.get('Authorization')
        if auth_header:
            try:
                auth_token = auth_header.split(" ")[1]
            except IndexError:
                return response('failed', 'Provide a valid auth token', 403)
            else:
                user_id = User.decode_auth_token(auth_token)  # change here
                user = User.get_by_id(user_id)
                if user:
                    user_devices = []
                    for dev in user.devices:
                        if dev.get_dev(2):
                            user_devices.append(dev.get_dev(2))
                    if not user_devices:
                        return response('No devices','No monitor device Found',400)
                    return response('Success', user_devices, 200)
                return response('Failed', 'user doest Exist', 400)
        return response('failed', 'Provide an authorization header', 403)


class user_get_control_devices(MethodView):
    def get(self):
        auth_header = request.headers.get('Authorization')
        if auth_header:
            try:
                auth_token = auth_header.split(" ")[1]
            except IndexError:
                return response('failed', 'Provide a valid auth token', 403)
            else:
                user_id = User.decode_auth_token(auth_token)  # change here
                user = User.get_by_id(user_id)
                if user:
                    user_devices = []
                    for dev in user.devices:
                        if dev.get_dev(1):
                            user_devices.append(dev.get_dev(1))
                    if not user_devices:
                        return response('No devices','No Control device Found',400)
                    return response('Success', user_devices, 200)
                return response('Failed', 'user doest Exist', 400)
        return response('failed', 'Provide an authorization header', 403)


class user_get_slider_devices(MethodView):
    def get(self):
        auth_header = request.headers.get('Authorization')
        if auth_header:
            try:
                auth_token = auth_header.split(" ")[1]
            except IndexError:
                return response('failed', 'Provide a valid auth token', 403)
            else:
                user_id = User.decode_auth_token(auth_token)  # change here
                user = User.get_by_id(user_id)
                if user:
                    user_devices = []
                    for dev in user.devices:
                        if dev.get_dev(3):
                            user_devices.append(dev.get_dev(3))
                    if not user_devices:
                        return response('No devices','No slider device Found',400)
                    return response('Success', user_devices, 200)
                return response('Failed', 'user doesnt Exist', 400)
        return response('failed', 'Provide an authorization header', 403)

config_dev = configure_devices.as_view('device')
dev.add_url_rule('/device', view_func=config_dev, methods=['POST','GET'])

monitor_dev = user_get_monitor_devices.as_view('device_monitor')
dev.add_url_rule('/device/monitor', view_func=monitor_dev, methods=['GET'])

control_dev = user_get_control_devices.as_view('device_control')
dev.add_url_rule('/device/control', view_func=control_dev, methods=['GET'])

slider_dev = user_get_slider_devices.as_view('device_slider')
dev.add_url_rule('/device/slider', view_func=slider_dev, methods=['GET'])
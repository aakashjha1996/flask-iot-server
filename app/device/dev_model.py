from app import db
from app.device_info.device_info_model import device_info
from app.device_type.device_type_model import device_type
import datetime


class device(db.Model):
    __tablename__= "device"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    dev_name = db.Column(db.String(255), nullable=False)
    dev_topic = db.Column(db.String(255), nullable=False)
    user_id = db.Column('user_id',db.Integer,db.ForeignKey('UserDetails.id'))
    device_type_id = db.Column('device_type_id', db.Integer, db.ForeignKey('device_type.id'))
    dev_max = db.Column(db.Integer)
    dev_min = db.Column(db.Integer)
    dev_step = db.Column(db.Integer)
    dev_info = db.relationship('device_info', backref='device')

    def __init__(self, dev_name, dev_topic, dev_type, dev_max, dev_min, dev_step):
        self.dev_name=dev_name
        self.dev_topic=dev_topic
        self.dev_max = dev_max
        self.dev_min = dev_min
        self.dev_step = dev_step
        dev_type_obj = device_type.query.filter_by(dev_type = dev_type).first()
        self.device_type_id = dev_type_obj.id
    # def __repr__(self):
    #     dev = {}
    #     dev['devname'] = self.dev_name
    #     dev['devtopic'] = self.dev_topic
    #     return str(dev)

    def save_dev(self, user):
        # db.session.add(self)

        user.devices.append(self)
        db.session.commit()
        return 'Device Added Successfully'

    #for admin
    def get_dev_data(self):
        dev = {}
        dev['devname'] = self.dev_name
        dev['devtopic'] = self.dev_topic
        dev['devstatus'] = None
        dev['devtime'] = None
        if self.device_type_id == 3:
            dev['devmax'] = self.dev_max
            dev['devmin'] = self.dev_min
            dev['devstep'] = self.dev_step
        # dev_info_object = device_info.query.filter_by(dev_id=self.id).order_by(device_info.id.desc()).first()
        # if not dev_info_object:
        #     dev['devstatus'] = 'off'
        #     timenow = datetime.datetime.now()
        #     timenow = str(timenow.date()) + ' ' + str(timenow.hour) + ':' + str(timenow.minute)
        #     dev['devtime'] = timenow
        # else:
        #     dev['devstatus'] = str(dev_info_object.dev_status)
        #     dev['devtime'] = str(dev_info_object.dev_status_time)
        dev_type_obj = device_type.query.filter_by(id=self.device_type_id).first()
        dev["devtype"] = dev_type_obj.dev_type
        return dev

    def get_dev(self, dev_type_id):
        dev = {}
        if self.device_type_id == dev_type_id:
            dev['devname'] = self.dev_name
            dev['devtopic'] = self.dev_topic
            if dev_type_id == 2:
                dev_info_object = device_info.query.filter_by(dev_id=self.id).order_by(device_info.id.desc()).all()
                if not dev_info_object:
                    dev['devstatus'] = [0]
                    timenow = datetime.datetime.now()
                    timenow = str(timenow.date()) + ' ' + str(timenow.hour) + ':' + str(timenow.minute)
                    dev['devtime'] = [timenow]
                else:
                    temp = []
                    devtime = []
                    if len(dev_info_object) < 10:
                        for dev_each_info in dev_info_object:
                            temp.append(int(dev_each_info.dev_status))
                            devtime.append(str(dev_each_info.dev_status_time))
                    else:
                        count = 0
                        for dev_each_info in dev_info_object:
                            if count < 10:
                                temp.append(int(dev_each_info.dev_status))
                                devtime.append(str(dev_each_info.dev_status_time))
                                count += 1
                    temp.reverse()
                    devtime.reverse()
                    dev['devstatus'] = temp
                    dev['devtime'] = devtime
            elif dev_type_id == 3:
                dev['devmax'] = self.dev_max
                dev['devmin'] = self.dev_min
                dev_info_object = device_info.query.filter_by(dev_id=self.id).order_by(device_info.id.desc()).first()
                if not dev_info_object:
                    dev['devstatus'] = self.dev_min
                    timenow = datetime.datetime.now()
                    timenow = str(timenow.date()) + ' ' + str(timenow.hour) + ':' + str(timenow.minute)
                    dev['devtime'] = timenow
                else:
                    dev['devstatus'] = str(dev_info_object.dev_status)
                    dev['devtime'] = str(dev_info_object.dev_status_time)
            else:

                dev_info_object = device_info.query.filter_by(dev_id=self.id).order_by(device_info.id.desc()).first()
                if not dev_info_object:
                    dev['devstatus'] = 'off'
                    timenow = datetime.datetime.now()
                    timenow = str(timenow.date()) + ' ' + str(timenow.hour) + ':' + str(timenow.minute)
                    dev['devtime'] = timenow
                else:
                    dev['devstatus'] = str(dev_info_object.dev_status)
                    dev['devtime'] = str(dev_info_object.dev_status_time)
        return dev


    def update_dev_data(self, new_devname, new_devtopic, new_devtype, new_devmax, new_devmin, new_devstep):
        self.dev_name = new_devname
        self.dev_topic = new_devtopic
        self.dev_max = new_devmax
        self.dev_min = new_devmin
        self.dev_step = new_devstep
        dev_type_obj = device_type.query.filter_by(dev_type=new_devtype).first()
        self.device_type_id = dev_type_obj.id
        db.session.commit()
        return 'Device Updated Successfully'

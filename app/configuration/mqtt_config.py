from flask_mqtt import Mqtt
from app import app
from app.auth.models import User, user_fcm_id
from app.device_info.device_info_model import device_info
from app.device.dev_model import device
import datetime
from app import socketio
from pyfcm import FCMNotification
from flask_socketio import emit
import eventlet
import json
eventlet.monkey_patch()
api_key = "AAAAAXMaKFI:APA91bHYNTHA-509K0eMXm1aKBRwp9iV0lq1K2VaUKrxL8rmddv5NgRsBswRJo3bfrT5KchHiBiiPhkye_wkURXSzSHgcuQGFyy44EviHfLMPM0CjH3uQ6c8UHXF3sgWTTSOwjMGPSjL"
push_service = FCMNotification(api_key=api_key)

class MQTT_Singleton(object):
    instance = None
    mqtt = None

    def __new__(cls):
        if MQTT_Singleton.instance is None:
            MQTT_Singleton.instance = object.__new__(cls)
        return MQTT_Singleton.instance

    def __init__(self):
        if MQTT_Singleton.mqtt is None:
            try:
                MQTT_Singleton.mqtt = Mqtt(app)
                print('MQTT Configured.')
            except:
                print 'Error while Connecting to MQTT Broker'
        else :
            print MQTT_Singleton.mqtt

mqtt = MQTT_Singleton().mqtt


@mqtt.on_connect()
def handle_connect(client, userdata, flags, rc):
    from app.auth.models import User
    user_object = User.query.all()
    if user_object:
        for each_user in user_object:
            email = str(each_user.email)
            if each_user.devices:
                for each_dev in each_user.devices:
                    if each_dev.device_type_id == 2:
                        topic = email + '/' +each_dev.dev_topic
                        mqtt.subscribe(topic)
    print 'Connected MQTT Broker'


@mqtt.on_message()
def handle_mqtt_message(client, userdata, message):
    data = dict(
        topic=message.topic,
        payload=message.payload.decode()
    )
    email , dev_topic = data['topic'].split('/')
    dev_topic
    user = User.get_by_email(email)
    devobj = device.query.filter_by(dev_topic=dev_topic, user_id=user.id).first()
    if devobj:
        #fcm notifcation
        user_fcm = user_fcm_id.query.filter_by(user_id=user.id).all()
        if user_fcm:
            fcm_reg_id = []
            message_title = 'Device : ' + str(devobj.dev_name)
            message_body = "Value : " + str(data['payload'])
            for each_user_fcm in user_fcm:
                fcm_reg_id.append(each_user_fcm.fcm_id)
            result = push_service.notify_multiple_devices(registration_ids=fcm_reg_id, message_title=message_title, message_body=message_body)
            print result
        device_info(data['payload']).save_devinfo(devobj)
        dev={}
        dev['devname'] = devobj.dev_name
        dev['devtopic'] = dev_topic
        dev['devstatus'] = int(data['payload'])
        timenow = datetime.datetime.now()
        timenow = str(timenow.date()) + ' ' + str(timenow.hour) + ':'+ str(timenow.minute)
        dev['devtime'] = timenow
        monitor_topic = email + '/monitor'
        socketio.emit(monitor_topic,json.dumps(dev))


@socketio.on('connect')
def handle_connect():
    print 'Incoming Connection request'


@socketio.on('device/slider')
def handle_message(slider_data):
    print('Received message: ' + str(slider_data))
    user = User.get_by_email(slider_data["email"])
    try:
        if slider_data["dev"]["devstatus"]:
            dev_status = slider_data["dev"]["devstatus"]
            dev_name = slider_data["dev"]["devname"]
            devobj = device.query.filter_by(dev_name=dev_name, user_id=user.id).first()
            if devobj:
                device_info(dev_status).save_devinfo(devobj)
                dev_mqtt_topic = slider_data["email"] + '/' + devobj.dev_topic
                print dev_mqtt_topic
                mqtt.publish(dev_mqtt_topic, dev_status)
    except Exception:
        user_devices = []
        for dev in user.devices:
            if dev.get_dev(3):
                user_devices.append(dev.get_dev(3))
        if not user_devices:
            socketio.emit('device/slider/send',json.dumps(user_devices))
        else :
            socketio.emit('device/slider/send', json.dumps(user_devices))

# @socketio.on('json')
# def handle_slider(json):
#     print('Received json: ' + str(json))


@socketio.on('disconnect')
def handle_disconnect():
    print 'Disconnected'


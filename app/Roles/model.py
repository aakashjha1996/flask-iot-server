from app import db


class Role(db.Model):
    __tablename__ = "Roles"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    rolename = db.Column(db.String(255), nullable=False)

    def __init__(self,rolename):
        self.rolename=rolename

    def getRoles(self):
        rolelist={'id':self.id,'roleName':self.rolename}
        return  rolelist

    def __repr__(self):
        return self.rolename
from flask import Blueprint, jsonify
from flask.views import MethodView
from app.Roles.model import Role
import json
role = Blueprint('role', __name__)


class GetRoles(MethodView):

    def get(self):
        roledetails=Role.query.all()
        rolelist = []
        for roles in roledetails:
          rolelist.append(roles.getRoles())
        return json.dumps(rolelist)



getRoles=GetRoles.as_view('roles')
role.add_url_rule('/roles', view_func=getRoles, methods=['GET'])

from flask import Blueprint, request
from flask.views import MethodView
from app.auth.helper import response
from app.auth.models import User
from app.device.dev_model import  device
from device_info_model import device_info
from app.configuration.mqtt_config import mqtt
dev_info = Blueprint('device_info', __name__)

class device_inf_view(MethodView):

    def post(self):
        if request.content_type == 'application/json':
            auth_header = request.headers.get('Authorization')
            if auth_header:
                try:
                    auth_token = auth_header.split(" ")[1]
                except IndexError:
                    return response('Failed', 'Provide a valid auth token', 403)
                else:
                    user_id = User.decode_auth_token(auth_token)
                    user_email = User.get_by_id(user_id).email
                    dev_infodata = request.get_json()
                    dev_status = dev_infodata.get('devstatus')
                    dev_name = dev_infodata.get('devname')
                    dev_topic=dev_infodata.get('devtopic')
                    mqtt.publish( user_email + '/' + dev_topic,dev_status)
                    devobj=device.query.filter_by(dev_name=dev_name,user_id=user_id).first()
                    print devobj
                    msg=device_info(dev_status).save_devinfo(devobj)
                    return response('success','Status Updated',200)
            return response('failed', 'Provide an authorization header', 403)
        return response('failed', 'Content-type must be json', 400)


devinfo = device_inf_view.as_view('device_info')
dev_info.add_url_rule('/device/deviceinfo', view_func=devinfo, methods=['POST'])
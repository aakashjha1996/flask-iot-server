from app import db
import datetime


class device_info(db.Model):
    __tablename__= "device_info"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    dev_status = db.Column(db.String(255), nullable=False)
    dev_status_time = db.Column(db.DateTime, nullable=False)
    dev_id = db.Column('dev_id',db.Integer,db.ForeignKey('device.id'))

    def __init__(self, dev_status):
        self.dev_status = dev_status
        timenow = datetime.datetime.now()
        timenow = str(timenow.date()) + ' ' + str(timenow.hour) + ':' + str(timenow.minute)
        self.dev_status_time = timenow

    def save_devinfo(self, device):
        # db.session.add(self)
        device.dev_info.append(self)
        db.session.commit()
        return 'Device Info Added Successfully'
import os

base_dir = os.path.abspath(os.path.dirname(__file__))
postgres_local_base = 'postgresql://postgres:root@123@localhost/'
database_name = 'test_db'


class BaseConfig:
    """
    Base application configuration
    """
    DEBUG = True
    SECRET_KEY = os.getenv('SECRET_KEY', 'my_strong_key')
    BCRYPT_HASH_PREFIX = 14
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    AUTH_TOKEN_EXPIRY_DAYS = 30
    AUTH_TOKEN_EXPIRY_SECONDS = 3000
    BUCKET_AND_ITEMS_PER_PAGE = 25


class DevelopmentConfig(BaseConfig):
    """
    Development application configuration
    """
    DEBUG = True
    # SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL', postgres_local_base + database_name)
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    BCRYPT_HASH_PREFIX = 4
    AUTH_TOKEN_EXPIRY_DAYS = 1
    AUTH_TOKEN_EXPIRY_SECONDS = 20
    BUCKET_AND_ITEMS_PER_PAGE = 4

    # MQTT CONFIGURATION
    MQTT_BROKER_URL = 'm14.cloudmqtt.com'  # use the free broker from HIVEMQ
    MQTT_BROKER_PORT = 10635  # default port for non-tls connection
    MQTT_USERNAME = 'buejxdyr'  # set the username here if you need authentication for the broker
    MQTT_PASSWORD = '14_7MM9-anPM'  # set the password here if the broker demands authentication
    MQTT_KEEPALIVE = 5  # set the time interval for sending a ping to the broker to 5 seconds
    MQTT_TLS_ENABLED = False  # set TLS to disabled for testing purposes

    SECRET_KEY = 'secretkey'

    MAIL_SERVER='smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USERNAME = 'centerfordigitalinnovation@gmail.com'
    MAIL_PASSWORD = 'cdi@Kengeri7'
    MAIL_USE_TLS = True
    # MAIL_USE_SSL = True

from app import app, db, bcrypt
import datetime
import jwt
from sqlalchemy.ext.declarative import declarative_base
# from Roles.model import Role


# Base = declarative_base()
user_role=db.Table('userDetails_Roles',#Base.metadata,
                   db.Column('id',db.Integer, primary_key=True, autoincrement=True),
                   db.Column('user_id',db.Integer,db.ForeignKey('UserDetails.id')),
                   db.Column('role_id',db.Integer,db.ForeignKey('Roles.id')))


class User(db.Model):
    """
    Table schema
    """
    __tablename__ = "UserDetails"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    registered_on = db.Column(db.DateTime, nullable=False)
    role=db.relationship('Role',secondary=user_role, backref=db.backref('Roles', lazy='dynamic'))
    devices=db.relationship('device',backref='UserDetails')
    user_fcm = db.relationship('user_fcm_id',backref='UserDetails')

    def __init__(self, email, password,username,role):
        self.email = email
        self.username=username
        self.password = bcrypt.generate_password_hash(password, app.config.get('BCRYPT_LOG_ROUNDS')) \
            .decode('utf-8')
        self.registered_on = datetime.datetime.now()
        import app.Roles.model as al
        a= al.Role.query.filter_by(rolename=role).first()
        self.role = [a]

    def save(self):
        """
        Persist the user in the database
        :param user:
        :return:
        """
        db.session.add(self)
        db.session.commit()
        user_d= {}
        user_d['token'] = self.encode_auth_token(self.id)
        user_d['user_obj'] = self
        return user_d

    def encode_auth_token(self, user_id):
        """
        Encode the Auth token
        :param user_id: User's Id
        :return:
        """
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=app.config.get('AUTH_TOKEN_EXPIRY_DAYS'),
                                                                       seconds=app.config.get(
                                                                           'AUTH_TOKEN_EXPIRY_SECONDS')),
                'iat': datetime.datetime.utcnow(),
                'sub': user_id
            }
            return jwt.encode(
                payload,
                app.config['SECRET_KEY'],
                algorithm='HS256'
            )
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(token):
        """
        Decoding the token to get the payload and then return the user Id in 'sub'
        :param token: Auth Token
        :return:
        """
        try:
            payload = jwt.decode(token, app.config['SECRET_KEY'], algorithms='HS256')
            is_token_blacklisted = BlackListToken.check_blacklist(token)
            if is_token_blacklisted:
                return 'Token was Blacklisted, Please login In'
            return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Signature expired, Please sign in again'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please sign in again'

    @staticmethod
    def get_by_id(user_id):
        """
        Filter a user by Id.
        :param user_id:
        :return: User or None
        """
        return User.query.filter_by(id=user_id).first()

    @staticmethod
    def get_by_email(email):
        """
        Check a user by their email address
        :param email:
        :return:
        """
        return User.query.filter_by(email=email).first()

    def reset_password(self, new_password):
        """
        Update/reset the user password.
        :param new_password: New User Password
        :return:
        """
        self.password = bcrypt.generate_password_hash(new_password, app.config.get('BCRYPT_LOG_ROUNDS')) \
            .decode('utf-8')
        db.session.commit()

    def get_user_data(self):
        user_data = {}
        user_data['email'] = self.email
        user_data['username'] = self.username
        return user_data


class BlackListToken(db.Model):
    """
    Table to store blacklisted/invalid auth tokens
    """
    __tablename__ = 'blacklist_token'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    token = db.Column(db.String(255), unique=True, nullable=False)
    blacklisted_on = db.Column(db.DateTime, nullable=False)

    def __init__(self, token):
        self.token = token
        self.blacklisted_on = datetime.datetime.now()

    def blacklist(self):
        """
        Persist Blacklisted token in the database
        :return:
        """
        db.session.add(self)
        db.session.commit()

    @staticmethod
    def check_blacklist(token):
        """
        Check to find out whether a token has already been blacklisted.
        :param token: Authorization token
        :return:
        """
        response = BlackListToken.query.filter_by(token=token).first()
        if response:
            return True
        return False


class user_fcm_id(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    fcm_id = db.Column(db.String(255), nullable=False)
    user_id = db.Column('user_id', db.Integer, db.ForeignKey('UserDetails.id'))

    def __init__(self, fcm_id):
        self.fcm_id = fcm_id

    def save_fcm_id(self, user):
        user.user_fcm.append(self)
        db.session.commit()
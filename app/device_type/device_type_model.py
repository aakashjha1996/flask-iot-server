from app import db


class device_type(db.Model):

    __tablename__ = "device_type"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    dev_type = db.Column(db.String(255), nullable=False)
    devices = db.relationship('device', backref='device_type')

    def __init__(self,dev_type):
        self.dev_type = dev_type

    def getDevType(self):
        dev_type_list={'id':self.id,'dev_type':self.dev_type}
        return dev_type_list

    def __repr__(self):
        return self.dev_type
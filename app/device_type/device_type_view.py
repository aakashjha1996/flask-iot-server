from flask import Blueprint, jsonify
from flask.views import MethodView
from app.device_type.device_type_model import device_type
import json
dev_type = Blueprint('device_type', __name__)

class GetDevice_type(MethodView):

    def get(self):
        dev_type_details = device_type.query.all()
        devtype_det = []
        for type in dev_type_details:
            devtype_det.append(type.getDevType())
        return json.dumps(devtype_det)

get_type = GetDevice_type.as_view('device_type')
dev_type.add_url_rule('/devicetype', view_func=get_type, methods=['GET'])
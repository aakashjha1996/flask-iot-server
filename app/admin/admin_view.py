from flask import Blueprint, request
from app.auth.helper import response, response_auth
from flask.views import MethodView
from app.auth.models import User
from app.device.dev_model import device
from app.device_type.device_type_model import device_type
from app import db, mail
from flask_mail import Message
from app.configuration.mqtt_config import mqtt

admin = Blueprint('admin', __name__)

class admin_control(MethodView):
    def get(self):
        auth_header = request.headers.get('Authorization')
        if auth_header:
            try:
                auth_token = auth_header.split(" ")[1]
            except IndexError:
                return response('failed', 'Provide a valid auth token', 403)
            else:
                user_id = User.decode_auth_token(auth_token)
                user_admin = User.get_by_id(user_id)
                if str(user_admin.role[0]) == 'ADMIN':
                    userlist = User.query.all()
                    user_details = []
                    for user in userlist:
                        if str(user.role[0]) == 'USER':
                            user_details.append(user.get_user_data())
                    return response('success',user_details,200)
                return response('Failure','Not ADMIN',400)
        return response('failed', 'Provide an authorization header', 403)

    def post(self):
        auth_header = request.headers.get('Authorization')
        if auth_header:
            try:
                auth_token = auth_header.split(" ")[1]
            except IndexError:
                return response('failed', 'Provide a valid auth token', 403)
            else:
                user_id = User.decode_auth_token(auth_token)
                user_admin = User.get_by_id(user_id)
                if str(user_admin.role[0]) == 'ADMIN':
                    user_email = request.get_json()
                    user_email = user_email.get('email')
                    user = User.get_by_email(user_email)
                    if user:
                        user_devices = []
                        if not user.devices:
                            return response('Success', 'No device present for user', 404)
                        for dev in user.devices:
                            user_devices.append(dev.get_dev_data())
                        return response('Success', user_devices, 200)
                    return response('Failed', 'user doest Exist', 400)
                return response('Failure', 'Not ADMIN', 400)
        return response('failed', 'Provide an authorization header', 403)


class admin_device_config(MethodView):

    def post(self):
        auth_header = request.headers.get('Authorization')
        if auth_header:
            try:
                auth_token = auth_header.split(" ")[1]
            except IndexError:
                return response('failed', 'Provide a valid auth token', 403)
            else:
                user_id = User.decode_auth_token(auth_token)
                user_admin = User.get_by_id(user_id)
                if str(user_admin.role[0]) == 'ADMIN':
                    dev_detail = request.get_json()
                    user_email = dev_detail.get('email')
                    dev_name = dev_detail.get('devname')
                    dev_topic = dev_detail.get('devtopic')
                    dev_type = dev_detail.get('devtype')
                    dev_max = dev_detail.get('devmax')
                    dev_min = dev_detail.get('devmin')
                    dev_step = dev_detail.get('devstep')
                    user = User.get_by_email(user_email)
                    if user:
                        message = device(dev_name, dev_topic, dev_type,dev_max,dev_min,dev_step).save_dev(user)
                        topic = user_email + '/' + dev_topic
                        msg = Message('Hello', sender='centerfordigitalinnovation@gmail.com', recipients=[user_email])
                        msg.body = topic
                        # mail.send(msg)
                        mqtt.subscribe(topic)
                        return response('Success', message, 200)
                    return response('Failed', 'user doest Exist', 400)
                return response('Failure', 'Not ADMIN', 400)
        return response('failed', 'Provide an authorization header', 403)

    def put(self):
        auth_header = request.headers.get('Authorization')
        if auth_header:
            try:
                auth_token = auth_header.split(" ")[1]
            except IndexError:
                return response('failed', 'Provide a valid auth token', 403)
            else:
                user_id = User.decode_auth_token(auth_token)
                user_admin = User.get_by_id(user_id)
                if str(user_admin.role[0]) == 'ADMIN':
                    dev_detail = request.get_json()
                    user_email = dev_detail.get('email')
                    dev_name = dev_detail.get('devname')
                    new_devname = dev_detail.get('new_devname')
                    new_devtopic = dev_detail.get('new_devtopic')
                    new_devtype = dev_detail.get('new_devtype')
                    user = User.get_by_email(user_email)
                    if user:
                        for dev in user.devices:
                            if dev_name == str(dev.dev_name):
                                dev_type_obj = device_type.query.filter_by(id=dev.device_type_id).first()
                                if new_devtype == dev_type_obj.dev_type:
                                    if dev.device_type_id == 3:
                                        new_devmax = dev_detail.get('new_devmax')
                                        new_devmin = dev_detail.get('new_devmin')
                                        new_devstep = dev_detail.get('new_devstep')
                                        message = dev.update_dev_data(new_devname,new_devtopic,new_devtype, new_devmax, new_devmin, new_devstep)
                                    else:
                                        message = dev.update_dev_data(new_devname,new_devtopic,new_devtype, None, None, None)
                                else:
                                    if new_devtype == 'slider':
                                        new_devmax = dev_detail.get('new_devmax')
                                        new_devmin = dev_detail.get('new_devmin')
                                        new_devstep = dev_detail.get('new_devstep')
                                        message = dev.update_dev_data(new_devname, new_devtopic, new_devtype, new_devmax, new_devmin, new_devstep)
                                    else:
                                        message = dev.update_dev_data(new_devname, new_devtopic, new_devtype, None, None, None)
                                return response('Success', message, 204)
                    return response('Failed', 'user doest Exist', 400)
                return response('Failure', 'Not ADMIN', 400)
        return response('failed', 'Provide an authorization header', 403)

    def delete(self):
        auth_header = request.headers.get('Authorization')
        if auth_header:
            try:
                auth_token = auth_header.split(" ")[1]
            except IndexError:
                return response('failed', 'Provide a valid auth token', 403)
            else:
                user_id = User.decode_auth_token(auth_token)
                user_admin = User.get_by_id(user_id)
                if str(user_admin.role[0]) == 'ADMIN':
                    # dev_detail = request.get_json()
                    user_email = request.args.get('email')
                    dev_name = request.args.get('dev')
                    user = User.get_by_email(user_email)
                    if user:
                        for dev in user.devices:
                            if dev_name == str(dev.dev_name):
                                if dev.dev_info:
                                    for each_dev_info in dev.dev_info:
                                        db.session.delete(each_dev_info)
                                db.session.delete(dev)
                                db.session.commit()
                        return response('Success', 'Device sauccesfully Deleted', 204)
                    return response('Failed', 'user doest Exist', 400)
                return response('Failure', 'Not ADMIN', 400)
        return response('failed', 'Provide an authorization header', 403)

admin_v = admin_control.as_view('admin')
admin_config = admin_device_config.as_view('admin_config')
admin.add_url_rule('/admin/userdetails', view_func=admin_v, methods=['GET','POST'])
admin.add_url_rule('/admin/deviceconfig', view_func=admin_config, methods=['POST','PUT','DELETE'])
import os

from flask import Flask, current_app
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_socketio import SocketIO
from flask_mail import Mail

# Initialize application
app = Flask(__name__, static_folder=None)

# Enabling cors
CORS(app)

# app configuration
app_settings = os.getenv(
    'APP_SETTINGS',
    'app.config.DevelopmentConfig'
)
app.config.from_object(app_settings)

# Initialize SocketIO
socketio = SocketIO(app)

# Initialize Bcrypt
bcrypt = Bcrypt(app)

# Initialize Flask Sql Alchemy
db = SQLAlchemy(app)

#include flask_mail
mail = Mail(app)

from Roles import model
from app.auth.models import User, user_fcm_id
from app.device import dev_model
from app.device_info import device_info_model
from app.device_type import device_type_model

#creating all the tables
db.create_all()

# Import the application views
from app import views

# Register blue prints
from app.auth.views import auth
app.register_blueprint(auth)

from app.Roles.views import role
app.register_blueprint(role)

from app.device.dev_view import dev
app.register_blueprint(dev)

from app.admin.admin_view import admin
app.register_blueprint(admin)

from app.device_info.device_info_view import dev_info
app.register_blueprint(dev_info)

from app.device_type.device_type_view import dev_type
app.register_blueprint(dev_type)

from app.configuration.mqtt_config import mqtt

# print 'Before SocketIO'
# socketio.run(app, use_reloader=False)
# with app.app_context():
#     print current_app.name
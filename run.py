from app import app, socketio

if __name__ == '__main__':
    socketio.run(app, debug="true", host="0.0.0.0", use_reloader=False)
    # app.run(debug="true",host="0.0.0.0")
